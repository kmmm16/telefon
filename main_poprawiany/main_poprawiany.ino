//zmienne, w sumie to malo istotne i  przesadnie nazwane//
float czasPomiedzyPikami = 0;
float aktualnyCzas = 0;
float czasWczesniejszejIteracji = 0;
float czasWykrycia = 0;
float czasUtrzymaniaStanuWysokiego = 0;
int state = 0;
int indexWybranegoNumeru = 0;
int aktualnaCyfraNumeru = 0;
int ilosc = 0;
boolean rozpoczetoOdczytPojedynczejCyfry = false;
boolean wykrytoPierwszyPik = false;
boolean przekroczonoCzasOczekiwaniaNaPikKolejny = false;
int wybranyNumer[] = {
  0,
  0,
  0,
  0
};
int cykl = 0;

//koniec zmiennych//

//SEKCJA KONFIGURACJI
const int pinDoPWM = 9;
const int czestotliwoscPWM = 400;
const int pinWejsciaCyfrowego = 8;

const float dozwolonyCzasPomiedzyPikami = 400; //[ms]
const float czasOdlozeniaSluchawki = 1000; //[ms]
const float progNapieciaPrzyPiku = 4.5;
const int debounceDelayPoZnalezieniuPiku = 90; //[ms]
const int poprawnyNumer[] = {
  10,
  5,
  1,
  7
};
const int pinWejsciowy = 13;

void setup() {
  pinMode(pinWejsciowy, INPUT_PULLUP);
  pinMode(pinDoPWM, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  aktualnyCzas = millis();

 // Glowna maszyn stanu.
 sprawdzCzyNieOdlozonoSluchawki(); // caly czas sprawdzane
 
  switch(state){
    case 0:
      resetNumeru();
      oczekiwanieNaPodniesienieSluchawki();
      break;
    case 1:
      diagnostyka();
      zbierzWybranyNumer(); //problematyczny fragment kodu
      break;
    case 2:
    sprawdzenieNumeru();
      break;
    case 3:  
      dzwon();
      oczekujNaStanWysokiWejscia();
      break;
    case 4:
    //dalsza logika
      break;
    
  }
     cykl++;
    czasWczesniejszejIteracji = aktualnyCzas;
}

void zlicanieNumeruPoPodniesienuSluchawki()
{
    if(OdczytWejsciaAnalogowego()>4.5)
    {
      aktualnaCyfraNumeru++;
      delay(90);
    }
}

void diagnostyka()
{
   
    
    if(cykl>5000)
    {
     Serial.println(aktualnaCyfraNumeru);
       
  Serial.println(wybranyNumer[0]);
  Serial.println(wybranyNumer[1]);
  Serial.println(wybranyNumer[2]);
  Serial.println(wybranyNumer[3]);
    cykl= 0; 
    }
}

void oczekiwanieNaPodniesienieSluchawki() // Stan 1
{
    
   float zasilanie = OdczytWejsciaAnalogowego();
   if(zasilanie < progNapieciaPrzyPiku) 
   {
    Serial.println("Podniesiono sluchawke");
    state++;
    delay(500);
    }
}
float OdczytWejsciaAnalogowego()
{
  int sensorValue = analogRead(A0);
  return sensorValue * (5.0 / 1023.0);
}

boolean OdczytWejsciaCyfrowego()
{
  return digitalRead(pinWejsciaCyfrowego);
}



void dzwon()
{
  tone(pinDoPWM, czestotliwoscPWM);
  delay(1000);
  noTone(pinDoPWM);
}

void sprawdzCzyNieOdlozonoSluchawki()
{
  //Sprawdzanie czy nie odlozono sluchawki
  

  if(aktualnyCzas > czasWczesniejszejIteracji)
  {
    if(OdczytWejsciaAnalogowego() > progNapieciaPrzyPiku)
    {
      czasUtrzymaniaStanuWysokiego += aktualnyCzas - czasWczesniejszejIteracji;
      if(czasUtrzymaniaStanuWysokiego > czasOdlozeniaSluchawki)
      {
        Serial.println("Odlozono sluchawke");
        state=0;
        czasUtrzymaniaStanuWysokiego = 0;
      }
    }
    else
    {
      czasUtrzymaniaStanuWysokiego = 0;
    }
  }
}
void zbierzWybranyNumer()
{


    if(aktualnyCzas > czasWczesniejszejIteracji)
    {
      //Oczekiwanie na pierwszy pik
      if(OdczytWejsciaAnalogowego()>progNapieciaPrzyPiku && wykrytoPierwszyPik == false)
      {
        //wyzerujLicznikiCzasu();
        aktualnaCyfraNumeru = 1;
        wykrytoPierwszyPik = true;
        czasWykrycia = millis();
        delay(debounceDelayPoZnalezieniuPiku);
      }
      
    else if(wykrytoPierwszyPik == true)
      {
       czasPomiedzyPikami += aktualnyCzas - czasWczesniejszejIteracji;
       if(czasPomiedzyPikami <= dozwolonyCzasPomiedzyPikami && czasPomiedzyPikami > 0 && OdczytWejsciaAnalogowego()>progNapieciaPrzyPiku)
       {
          aktualnaCyfraNumeru++;
          czasPomiedzyPikami = 0;
          delay(debounceDelayPoZnalezieniuPiku);
       }
       else if(czasPomiedzyPikami > dozwolonyCzasPomiedzyPikami)
       {
          czasPomiedzyPikami = 0;
          wybranyNumer[indexWybranegoNumeru] = aktualnaCyfraNumeru;
          wykrytoPierwszyPik = false;
              if(indexWybranegoNumeru == 3)
              {
                indexWybranegoNumeru = 0;
                state++;
              }
          aktualnaCyfraNumeru = 0;
          indexWybranegoNumeru++;
        }
      }
    }
}

void wyzerujLicznikiCzasu()
{
  czasWczesniejszejIteracji = 0;
  aktualnyCzas = 0;
}

void resetNumeru()
{
  wybranyNumer[0] = 0;
  wybranyNumer[1] = 0;
  wybranyNumer[2] = 0;
  wybranyNumer[3] = 0;
  aktualnaCyfraNumeru = 0;
  indexWybranegoNumeru = 0;
}

void sprawdzenieNumeru()
{
  bool error = false;
  
  Serial.println(wybranyNumer[0]);
  Serial.println(wybranyNumer[1]);
  Serial.println(wybranyNumer[2]);
  Serial.println(wybranyNumer[3]);
  
  for(int i = 0; i < 4; i++)
  {
    if(wybranyNumer[i] != poprawnyNumer[i]) error = true;   
  }
  if(error) 
  {
    
    Serial.println("chuj");state = 0;
  }
  else state++;
}

void oczekujNaStanWysokiWejscia()
{
  int val = digitalRead(pinWejsciowy);
  if(val == 0) state==0;
}
