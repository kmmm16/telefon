//zmienne, w sumie to malo istotne i  przesadnie nazwane
float czasPomiedzyPikami = 0;
float aktualnyCzas = 0;
float czasWczesniejszejIteracji = 0;
float czasWykrycia = 0;
float czasUtrzymaniaStanuWysokiego = 0;
int state = 0;
int indexWybranegoNumeru = 0;
int aktualnaCyfraNumeru = 0;
int ilosc = 0;
boolean rozpoczetoOdczytPojedynczejCyfry = false;
boolean wykrytoPierwszyPik = false;
boolean przekroczonoCzasOczekiwaniaNaPikKolejny = false;
int wybranyNumer[] = {
  0,
  0,
  0,
  0
};
// Nazwijmy to: SEKCJA KONFIGURACJI
const int pinDoPWM = 9;
const int czestotliwoscPWM = 400;
const float dozwolonyCzasPomiedzyPikami = 1500; //[ms]
const int pinWejsciaCyfrowego = 8;
const float czasOdlozeniaSluchawki = 5000; //[ms]
const float progNapieciaPrzyPiku = 3.2/10.0;
const int poprawnyNumer[] = {
  6,
  6,
  6,
  6
};
const int pinWejsciowy = 13;

void setup() {
  pinMode(pinWejsciowy, INPUT);
  pinMode(pinDoPWM, OUTPUT);
  Serial.begin(9600);
}

void loop() {

 // Glowna maszyn stanu.
  switch(state){
    case 0:
      resetNumeru();
      oczekiwanieNaPodniesienieSluchawki();
      break;
    case 1:
      sprawdzCzyNieOdlozonoSluchawki();
      zbierzWybranyNumer();
      break;
    case 2:
      sprawdzenieNumeru();
      break;
    case 3:
      dzwon();
      oczekujNaStanWysokiWyjscia();   
      break;
    case 4:
    //dalsza logika
      break;
    
  }
}

void oczekiwanieNaPodniesienieSluchawki() // Stan 1
{
    
   float zasilanie = OdczytWejsciaAnalogowego();
   if(zasilanie < progNapieciaPrzyPiku) 
   {
    Serial.println("Podniesiono sluchawke");
    state++;
    }
}
float OdczytWejsciaAnalogowego()
{
  int sensorValue = analogRead(A0);
  return sensorValue * (5.0 / 1023.0);
}

boolean OdczytWejsciaCyfrowego()
{
  return digitalRead(pinWejsciaCyfrowego);
}



void dzwon()
{
  tone(pinDoPWM, czestotliwoscPWM);
  delay(1000);
  noTone(pinDoPWM);
}

void sprawdzCzyNieOdlozonoSluchawki()
{
  //Sprawdzanie czy nie odlozono sluchawki
  aktualnyCzas = millis();

  if(aktualnyCzas > czasWczesniejszejIteracji)
  {
    if(OdczytWejsciaAnalogowego() > progNapieciaPrzyPiku)
    {
      czasUtrzymaniaStanuWysokiego += aktualnyCzas - czasWczesniejszejIteracji;
      Serial.println(aktualnaCyfraNumeru);
        Serial.println(czasPomiedzyPikami);
        Serial.println("bum");
        Serial.println(czasUtrzymaniaStanuWysokiego);
        Serial.println(czasOdlozeniaSluchawki);
      if(czasUtrzymaniaStanuWysokiego > czasOdlozeniaSluchawki)
      {
        Serial.println("Odlozono sluchawke");
        state--;
        czasUtrzymaniaStanuWysokiego = 0;
      }
    }
    else
    {
      czasUtrzymaniaStanuWysokiego = 0;
    }
  }
}
void zbierzWybranyNumer()
{


    if(aktualnyCzas > czasWczesniejszejIteracji)
    {
      //Oczekiwanie na pierwszy pik
      if(OdczytWejsciaAnalogowego()>progNapieciaPrzyPiku && wykrytoPierwszyPik == false)
      {
        //wyzerujLicznikiCzasu();
        aktualnaCyfraNumeru = 1;
        wykrytoPierwszyPik = true;
        czasWykrycia = millis();
        delay(300);
      }

        //Działa jak chuj w dupie policji ;).
    else if(wykrytoPierwszyPik == true)
      {
       czasPomiedzyPikami += aktualnyCzas - czasWczesniejszejIteracji;
       if(czasPomiedzyPikami <= dozwolonyCzasPomiedzyPikami && czasPomiedzyPikami > 0 && OdczytWejsciaAnalogowego()>progNapieciaPrzyPiku)
       {
          aktualnaCyfraNumeru++;
          czasPomiedzyPikami = 0;
          delay(300);
       }
       else if(czasPomiedzyPikami > dozwolonyCzasPomiedzyPikami)
       {
          czasPomiedzyPikami = 0;
          wybranyNumer[indexWybranegoNumeru] = aktualnaCyfraNumeru;
          wykrytoPierwszyPik = false;
              if(indexWybranegoNumeru == 3)
              {
                indexWybranegoNumeru = 0;
                state++;
              }
          aktualnaCyfraNumeru = 0;
          indexWybranegoNumeru++;
        }
      }
    }
    
  czasWczesniejszejIteracji = aktualnyCzas;
}

void wyzerujLicznikiCzasu()
{
  czasWczesniejszejIteracji = 0;
  aktualnyCzas = 0;
}

void resetNumeru()
{
  wybranyNumer[0] = 0;
  wybranyNumer[1] = 0;
  wybranyNumer[2] = 0;
  wybranyNumer[3] = 0;
}

void sprawdzenieNumeru()
{
  bool error = false;
  
  Serial.println(wybranyNumer[0]);
  Serial.println(wybranyNumer[1]);
  Serial.println(wybranyNumer[2]);
  Serial.println(wybranyNumer[3]);
  
  for(int i = 0; i < 4; i++)
  {
    if(wybranyNumer[i] != poprawnyNumer[i]) error = true;   
  }
  if(error) 
  {
    state = state - 2;
  }
  else state++;
}

void oczekujNaStanWysokiWyjscia()
{
  int val = digitalRead(pinWejsciowy);
  if(val == 1) state++;
}

