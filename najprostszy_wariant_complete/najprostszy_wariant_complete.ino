//zmienne, w sumie to malo istotne i  przesadnie nazwane//
float czasPomiedzyPikami = 0;
float aktualnyCzas = 0;
float czasWczesniejszejIteracji = 0;
float czasWykrycia = 0;
float czasUtrzymaniaStanuWysokiego = 0;
int state = 0;
int indexWybranegoNumeru = 0;
int aktualnaCyfraNumeru = 0;
int ilosc = 0;
boolean rozpoczetoOdczytPojedynczejCyfry = false;
boolean wykrytoPierwszyPik = false;
boolean przekroczonoCzasOczekiwaniaNaPikKolejny = false;

int wybranyNumer[] = {
  0,
  0,
  0,
  0
};
int cykl = 0;

//koniec zmiennych//

//SEKCJA KONFIGURACJI
const int pinDoPWM = 9;
const int czestotliwoscPWM = 400;
const int pinWejsciaCyfrowego = 8;

const float dozwolonyCzasPomiedzyPikami = 400; //[ms]
const float czasOdlozeniaSluchawki = 1000; //[ms]
const float progNapieciaPrzyPiku = 4.5;
const int debounceDelayPoZnalezieniuPiku = 90; //[ms]
const int poprawnyNumer[] = {
  10,
  5,
  1,
  7
};
const int pinWejsciowy = 13;

void setup() {
  pinMode(pinWejsciowy, INPUT_PULLUP);
  pinMode(pinDoPWM, OUTPUT);
}

void loop() {
  
 // Glowna maszyn stanu.
 sprawdzCzyNieOdlozonoSluchawki(); // caly czas sprawdzane
 
  switch(state){
    case 0:
      oczekiwanieNaPodniesienieSluchawki();
      break;
    case 1:
      dzwon();
      oczekujNaStanWysokiWejscia();
      break; 
  }
}

void oczekiwanieNaPodniesienieSluchawki() // Stan 1
{
    
   float zasilanie = OdczytWejsciaAnalogowego();
   if(zasilanie < progNapieciaPrzyPiku) 
   {
    state++;
    delay(500);
    }
}
float OdczytWejsciaAnalogowego()
{
  int sensorValue = analogRead(A0);
  return sensorValue * (5.0 / 1023.0);
}

boolean OdczytWejsciaCyfrowego()
{
  return digitalRead(pinWejsciaCyfrowego);
}

void dzwon()
{
  tone(pinDoPWM, czestotliwoscPWM);
  delay(300);
  noTone(pinDoPWM);
}

void sprawdzCzyNieOdlozonoSluchawki()
{
  //Sprawdzanie czy nie odlozono sluchawki

  if(aktualnyCzas > czasWczesniejszejIteracji)
  {
    if(OdczytWejsciaAnalogowego() > progNapieciaPrzyPiku)
    {
        czasUtrzymaniaStanuWysokiego += aktualnyCzas - czasWczesniejszejIteracji;
        if(czasUtrzymaniaStanuWysokiego > czasOdlozeniaSluchawki)
        {
          state=0;
          czasUtrzymaniaStanuWysokiego = 0;
        }
    }
  }
}

void oczekujNaStanWysokiWejscia()
{
  int val = digitalRead(pinWejsciowy);
  if(val == 0) state==0;
}
